import scene from './js/scene'
import './css/main.scss';
import scene2 from './js/scene2';
import scene3 from './js/scene3';
import scene4 from './js/scene4';
import scene5 from './js/scene5';
import scene6 from './js/scene6';
import scene7ShaderIntro from './js/scene7-shader-intro';
import mosaicShader from './js/mosaicShader';
import object3d from './js/3dObject';
import video from './js/video';
import videoCanvas from './js/videoCanvas';

let links = document.querySelectorAll('a');
links.forEach(link => {
	if (window.location.href == link.href) {
		link
			.classList
			.add('-active');
		return;
	}
	link.addEventListener('click', event => {
		setTimeout(() => {
			window
				.location
				.reload();
		})
	})
})
document.addEventListener('DOMContentLoaded', () => {
	switch (window.location.hash) {
		case '#ruby':
			scene7ShaderIntro();
			break;
		case '#randomPlace':
			scene5();
			break;
		case '#rubyMask':
			mosaicShader();
			break;
		case '#3d-object':
			object3d();
			break;
		case '#video':
			video();
			break;
		case '#videocanvas':
			videoCanvas();
			break;
		default:
			scene6();
			break;
	}
})