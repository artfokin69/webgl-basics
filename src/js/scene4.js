//АНИМАЦИЯ ПОВЕРХНОСТИ

import * as THREE from 'three';
import Controls from './Controls';
import Perlin from './perlin';

var camera, scene, renderer, mesh;
var gridSize = 120;
var time = 0;

function init() {
	scene = new THREE.Scene();
	scene.background = new THREE.Color(0x000000);
	renderer = new THREE.WebGLRenderer();

	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);

	camera = new THREE.PerspectiveCamera(
		90,
		window.innerWidth / window.innerHeight,
		1,
		3000
	)
	camera.position.z = 200;
	camera.position.x = -200;
	camera.position.y = -200;

	Controls({
		scene,
		camera,
		renderer
	});

	document.body.appendChild(renderer.domElement);


	// ANYTING

	var material = new THREE.ShaderMaterial({
		transparent: true,
		// wireframe:true,
		extensions: {
			derivatives: '#extension GL_OES_standart_derivatives : enable',
		},
		uniforms: {
			time: {
				type: 'f',
				value: 0.0
			},
		},
		vertexShader: document.getElementById('vertShader4').textContent,
		fragmentShader: document.getElementById('fragShader4').textContent,
		side: THREE.DoubleSide,
	});


	var geometry = new THREE.PlaneGeometry(600, 600, gridSize, gridSize);

	mesh = new THREE.Mesh(geometry, material);
	scene.add(mesh);
	//END ANYTING




	animate();
	window.addEventListener('resize', resize);

}

function animate() {
	time++;
	updatePlane();
	window.requestAnimationFrame(animate);
	renderer.render(scene, camera);
}

function resize() {
	renderer.setSize(window.innerWidth, window.innerHeight);
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
}

function updatePlane() {
	let V = mesh.geometry.vertices;
	for (let i = 0; i < V.length; i++) {
		const v = V[i];
		v.z = 100 * Perlin(v.x / 100, v.y / 100, time / 100)
	}
	mesh.geometry.verticesNeedUpdate = true;
}











export default init;