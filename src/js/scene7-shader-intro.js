// https://habr.com/ru/post/420847/
import * as dat from 'dat.gui';



const IDs = {
	canvas: 'canvas',
	shaders: {
		vertex: 'vertShader7',
		fragment: 'fragShader7'
	}
};
const imgUrl = 'ruby.jpg';

let CANVAS, GL, PROGRAM, MOUSE_POSITION;
let IS_ACTIVE = true;
let INTENSITY = 1;
let userSettings = {
	isGlitch: false,
	isWaves: false,
	isZoom: false,
	glitchWidth: 1000,
	glitchShift: 1
}

function createPlane() {
	GL.bindBuffer(GL.ARRAY_BUFFER, GL.createBuffer());
	GL.bufferData(GL.ARRAY_BUFFER, new Float32Array([
		-1,
		-1,
		-1,
		1,
		1,
		-1,
		1,
		1
	]), GL.STATIC_DRAW);
}

function createProgram() {
	const shaders = getShaders();

	PROGRAM = GL.createProgram();

	GL.attachShader(PROGRAM, shaders.vertex);
	GL.attachShader(PROGRAM, shaders.fragment);
	GL.linkProgram(PROGRAM);

	const vertexPositionAttribute = GL.getAttribLocation(PROGRAM, 'a_position');
	GL.getUniformLocation(PROGRAM, 'u_time')
	GL.enableVertexAttribArray(vertexPositionAttribute);
	GL.vertexAttribPointer(vertexPositionAttribute, 2, GL.FLOAT, false, 0, 0);

	GL.useProgram(PROGRAM);
}

function getShaders() {
	return {
		vertex: compileShader(GL.VERTEX_SHADER, document.getElementById(IDs.shaders.vertex).textContent),
		fragment: compileShader(GL.FRAGMENT_SHADER, document.getElementById(IDs.shaders.fragment).textContent)
	};
}

function compileShader(type, source) {
	const shader = GL.createShader(type);

	GL.shaderSource(shader, source);
	GL.compileShader(shader);

	console.log(GL.getShaderInfoLog(shader));

	return shader;
}

function updateCanvasSize() {
	const size = Math.ceil(Math.max(window.innerHeight, window.innerWidth))

	CANVAS.height = size;
	CANVAS.width = size;

	GL.viewport(0, 0, GL.canvas.width, GL.canvas.height);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_canvas_size'), Math.max(CANVAS.height, CANVAS.width));
}

function initEventListeners() {
	window.addEventListener('resize', updateCanvasSize);
	CANVAS.addEventListener('mouseover', () => {
		IS_ACTIVE = false;
	});

	CANVAS.addEventListener('mouseout', () => {
		IS_ACTIVE = true;
	});

	document.addEventListener('mousemove', (e) => {
		let rect = CANVAS.getBoundingClientRect();

		MOUSE_POSITION = [
			e.clientX - rect.left,
			rect.height - (e.clientY - rect.top)
		];

		GL.uniform2fv(GL.getUniformLocation(PROGRAM, 'u_mouse_position'), MOUSE_POSITION);
	});
}

function createTexture() {
	const image = new Image();

	image.crossOrigin = 'anonymous';

	image.onload = () => {
		const texture = GL.createTexture();
		GL.activeTexture(GL.TEXTURE0);
		GL.bindTexture(GL.TEXTURE_2D, texture);
		GL.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, true);
		GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGB, GL.RGB, GL.UNSIGNED_BYTE, image);
		//там где текстуры не хватило дополняем растягивая пиксели краев текстуры
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);

		//передаем в шейдер
		GL.uniform1i(GL.getUniformLocation(PROGRAM, 'u_texture'), 0);
	};

	image.src = 'ruby.jpg';
}

function draw(timeStamp) {
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_time'), timeStamp / 1000.0);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_intensity'), INTENSITY);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_isGlitch'), userSettings.isGlitch ?
		1 :
		0);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_isWaves'), userSettings.isWaves ?
		1 :
		0);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_isZoom'), userSettings.isZoom ?
		1 :
		0);

	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_glitchWidth'), userSettings.glitchWidth);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_glitchShift'), userSettings.glitchShift);

	if (userSettings.isWaves) {
		if (IS_ACTIVE) {
			GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);

			if (INTENSITY < 1) {
				INTENSITY += 0.02;
			}
		} else {
			if (INTENSITY > 0) {
				INTENSITY -= 0.02;
				GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
			}
		}
	} else {
		INTENSITY = 1;
	}

	GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
	requestAnimationFrame(draw);
}

function initGui() {
	const gui = new dat.GUI();
	gui.add(userSettings, "isGlitch")
	gui.add(userSettings, "isWaves")
	gui.add(userSettings, "isZoom")

	gui.add(userSettings, "glitchWidth", 1, 1000);
	gui.add(userSettings, "glitchShift", 0, 1);
}

function initCanvas() {
	CANVAS = document.createElement('canvas');
	document.body.appendChild(CANVAS);
	GL = CANVAS.getContext('webgl');
}

export default function main() {
	initCanvas();
	createPlane();
	createProgram();
	createTexture();
	updateCanvasSize();
	initEventListeners();
	draw();
	initGui();
}