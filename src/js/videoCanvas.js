var video;
var canvas;

function startPlayback() {

	video = document.createElement('video');
	video.src = 'video_mobile.mp4';
	video.loop = true;
	video.muted = true;
	video.playsInline = true;
	video.crossOrigin = 'anonymous';
	video.addEventListener('playing', paintVideo);

	video.play().then(() => {
			alert('native video supported');
		})
		.catch(() => {
			alert('native video not supported');
		});
}

function paintVideo() {
	if (!canvas) {
		canvas = document.createElement('canvas');
		canvas.width = video.videoWidth;
		canvas.height = video.videoHeight;
		document.body.appendChild(canvas);
	}
	canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
	if (!video.paused)
		requestAnimationFrame(paintVideo);
}

export default startPlayback;

// var videoSrc = 'video_mobile.mp4';
// let video;

// const canvas = document.getElementById('canvas');
// const ctx = canvas.getContext('2d');

// export default function () {
// 	video = document.createElement('video');
// 	console.log(canvas, ctx);
// 	video.width = window.innerWidth;
// 	video.height = window.innerHeight;
// 	canvas.width = window.innerWidth;
// 	canvas.height = window.innerHeight;
// 	canvas.classList.add('-fullScreen');
// 	video.loop = true;
// 	video.muted = true;
// 	video.playsinline = true;

// 	// video.autoplay = true;
// 	// video.loop = true;
// 	// video.playsinline = true;
// 	// video.muted = true;

// 	// document.body.appendChild(video);
// 	video.src = videoSrc;
// 	video.play();
// 	video.addEventListener('canplaythrough', videoLoaded)
// 	// video.addEventListener('playing', paintVideo);
// }

// function videoLoaded() {
// 	video.removeEventListener('canplaythrough', videoLoaded);
// 	paintVideo();
// 	// video.play()
// 	// 	.then(() => {
// 	// 		alert('native video supported');
// 	// 	})
// 	// 	.catch(() => {
// 	// 		alert('native video not supporte');
// 	// 		ww = new Whitewater(canvas, videoSrc, {
// 	// 			autoplay: true,
// 	// 			loop: true,
// 	// 			controls: false,
// 	// 			muted: true,
// 	// 		})
// 	// 	})
// }



// function paintVideo() {
// 	console.log(canvas, canvas.getContext)
// 	ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
// 	if (!video.paused) {
// 		requestAnimationFrame(paintVideo);
// 	}
// }