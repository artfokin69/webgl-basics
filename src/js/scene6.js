//https://www.youtube.com/watch?v=biZgx45Mzqo&t=4s

import * as THREE from 'three';
import Controls from './Controls';
import Perlin from './perlin';
import * as dat from 'dat.gui';

let userSettings = {
	speed: 1,
	amplitude: 79,
	period: 6,
	logMeshRotate: false,
	logCameraPosition: false,
	logScenePosition: false,
	animate: false,
	messages: [
		'Use Z key for rotate Mesh around Z axis, Shift + Z for back rotate, similarly fo' +
		'r X and Y keys.',
		'Use R key for set original Mesh rotate state.'
	]
}

var camera,
	scene,
	renderer,
	mesh,
	material,
	camera_pivot,
	rotateAxis;
var gridSize = 200;
var planeW = 1000;
var planeH = 1000;
var time = 1000;
const toRad = THREE.Math.degToRad;
const toDeg = THREE.Math.radToDeg;

function init() {
	scene = new THREE.Scene();
	scene.background = new THREE.Color(0x000000);
	renderer = new THREE.WebGLRenderer({
		//		antialias: true
	});

	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);

	camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 1, 3000)

	camera
		.position
		// .set(-122, -500, 147)
		.set(74, -409, 186);

	// camera_pivot = new THREE.Object3D()
	// rotateAxis = new THREE.Vector3(0, 0, 1);
	// scene.add(camera_pivot);
	// camera_pivot.add(camera);
	// camera.lookAt(camera_pivot.position);
	// camera_pivot.rotateOnAxis(rotateAxis, 100 * Math.PI / 180);
	document
		.body
		.appendChild(renderer.domElement);

	// ANYTING
	var texture = THREE
		.ImageUtils
		.loadTexture('lines-2k-wrapped.jpg');
	texture.center = new THREE.Vector2(0.5, 0.5);
	// texture.rotation = -1;
	material = new THREE.ShaderMaterial({
		// wireframe:true,
		extensions: {
			derivatives: '#extension GL_OES_standart_derivatives : enable'
		},
		uniforms: {
			time: {
				type: 'f',
				value: 0.0
			},
			texture1: {
				type: "t",
				value: texture
			},
			sizeX: {
				type: 'f',
				value: planeW
			},
			sizeY: {
				type: 'f',
				value: planeH
			},
			u_speed: {
				type: 'f',
				value: getSpeed(userSettings.speed)
			},
			u_amplitude: {
				type: 'f',
				value: userSettings.amplitude
			},
			u_period: {
				type: 'f',
				value: userSettings.period
			}
		},
		vertexShader: document
			.getElementById('vertShader6')
			.textContent,
		fragmentShader: document
			.getElementById('fragShader6')
			.textContent,
		side: THREE.DoubleSide
	});

	var geometry = new THREE.PlaneGeometry(planeW, planeH, gridSize, gridSize);
	mesh = new THREE.Mesh(geometry, material);
	mesh.rotation.z = 4.19;

	// mesh.position.x = planeW / 2; mesh.position.y = planeH / 2;
	scene.add(mesh);
	scene.rotation.z = toRad(270);
	// scene.rotation.z = 180 * Math.PI / 180; END ANYTING

	animate();
	window.addEventListener('resize', resize);

	// document.addEventListener('click',()=>{ console.log(camera.position); })

	Controls({
		scene,
		camera,
		renderer,
		axes: false,
		orbit: true,
		meshRotate: mesh
	});
	iniGui();
}

function animate() {
	if (userSettings.animate) {
		time++;
	}
	material.uniforms.time.value = time;
	material.uniforms.u_period.value = userSettings.period;
	material.uniforms.u_amplitude.value = userSettings.amplitude;
	material.uniforms.u_speed.value = getSpeed(userSettings.speed);
	window.requestAnimationFrame(animate);
	renderer.render(scene, camera);
	if (userSettings.logScenePosition) {
		console.log({
			x: scene.position.x,
			y: scene.position.y,
			z: scene.position.z
		})
	}
	if (userSettings.logMeshRotate) {
		console.log({
			x: mesh.rotation.x,
			y: mesh.rotation.y,
			z: mesh.rotation.z
		}, {
			x: mesh.position.x,
			y: mesh.position.y,
			z: mesh.position.z
		});
	}
	if (userSettings.logCameraPosition) {

		console.log({
			x: camera.position.x,
			y: camera.position.y,
			z: camera.position.z
		});
	}
	// console.log(camera.position, scene.rotation);

}

function resize() {
	renderer.setSize(window.innerWidth, window.innerHeight);
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
}

function getSpeed(speedCoef) {
	return speedCoef / 100;
}

function iniGui() {
	const gui = new dat.GUI();
	gui.add(userSettings, "speed", 0.1, 10);
	gui.add(userSettings, "amplitude", 10, 300);
	gui.add(userSettings, "period", 0.1, 20);
	gui.add(userSettings, "logMeshRotate");
	gui.add(userSettings, "logCameraPosition");
	gui.add(userSettings, "animate");

	let msWrap = document.createElement('ul');
	msWrap
		.classList
		.add('msContainer');
	document
		.body
		.appendChild(msWrap);
	userSettings
		.messages
		.forEach(e => {
			let ms = document.createElement('li');
			ms
				.classList
				.add('message');
			ms.textContent = e;
			msWrap.appendChild(ms);
		});
	msWrap.addEventListener('click', () => {
		msWrap
			.classList
			.toggle('-closed');
	})
}

export default init;