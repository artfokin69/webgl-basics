const IDs = {
	canvas: 'canvas',
	shaders: {
		vertex: 'videoVertShader',
		fragment: 'videoFragShader'
	}
};

const TEXTURE_URL = 'video_mobile.mp4'
var copyVideo = false;
var VIDEO, TEXTURE;
var VIDEO_W, VIDEO_H;
const CANVAS = document.getElementById(IDs.canvas);
const GL = canvas.getContext('webgl');
let PROGRAM;



export default function () {
	clearCanvas();
	createPlane();
	createProgram();
	setupVideo();
	createTexture();
	updateCanvasSize();
	initEventListeners();
	draw();
}

function clearCanvas() {
	GL.clearColor(0.26, 1, 0.93, 1.0);
	GL.clear(GL.COLOR_BUFFER_BIT);
}

function createPlane() {
	GL.bindBuffer(GL.ARRAY_BUFFER, GL.createBuffer());
	GL.bufferData(
		GL.ARRAY_BUFFER,
		new Float32Array([
			-1, -1,
			-1, 1,
			1, -1,
			1, 1
		]),
		GL.STATIC_DRAW
	);
}

function createShader(gl, sourceCode, type) {
	// Compiles either a shader of type gl.VERTEX_SHADER or gl.FRAGMENT_SHADER
	var shader = gl.createShader(type);
	gl.shaderSource(shader, sourceCode);
	gl.compileShader(shader);

	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		var info = gl.getShaderInfoLog(shader);
		throw 'Could not compile WebGL program. \n\n' + info;
	}
	return shader;
}

function createProgram() {
	const shaders = getShaders();
	const vertexShaderSource = `
		precision mediump float;

		attribute vec2 a_position;

		void main() {
				gl_Position = vec4(a_position, 0, 1);
		}`;
	const fragmentShaderSource = `
		precision mediump float;

		uniform sampler2D u_texture;
		uniform float u_canvas_size;
		uniform vec2 u_resolution;

		void main() {
				gl_FragColor = texture2D(u_texture, gl_FragCoord.xy/u_resolution);
		} `;

	PROGRAM = GL.createProgram();
	GL.attachShader(PROGRAM, createShader(GL, fragmentShaderSource, GL.FRAGMENT_SHADER));
	GL.attachShader(PROGRAM, createShader(GL, vertexShaderSource, GL.VERTEX_SHADER));
	GL.linkProgram(PROGRAM);

	const vertexPositionAttribute = GL.getAttribLocation(PROGRAM, 'a_position');

	GL.enableVertexAttribArray(vertexPositionAttribute);
	GL.vertexAttribPointer(vertexPositionAttribute, 2, GL.FLOAT, false, 0, 0);

	GL.useProgram(PROGRAM);
}

function getShaders() {
	return {
		vertex: compileShader(
			GL.VERTEX_SHADER,
			document.getElementById(IDs.shaders.vertex).textContent
		),
		fragment: compileShader(
			GL.FRAGMENT_SHADER,
			document.getElementById(IDs.shaders.fragment).textContent
		)
	};
}

function compileShader(type, source) {
	const shader = GL.createShader(type);

	GL.shaderSource(shader, source);
	GL.compileShader(shader);

	console.log(GL.getShaderInfoLog(shader));

	return shader;
}

function setupVideo() {
	VIDEO = document.createElement('video');

	var playing = false;
	var timeupdate = false;
	var loadedmetadata = false;

	VIDEO.autoplay = true;
	VIDEO.muted = true;
	VIDEO.loop = true;
	VIDEO.playsInline = true;
	VIDEO.crossOrigin = 'anonymous';
	//ожидание этиъ двух событий обеспечивает что в видео есть данные
	VIDEO.addEventListener('playing', function () {
		if (playing) return;
		playing = true;
		checkReady();
	}, true);

	VIDEO.addEventListener('loadedmetadata', function () {
		if (loadedmetadata) return;
		loadedmetadata = true;
		checkReady();
	}, true)

	VIDEO.addEventListener('timeupdate', function () {
		if (timeupdate) return;
		timeupdate = true;
		checkReady();
	}, true);

	VIDEO.src = TEXTURE_URL;
	VIDEO.play().then(() => {
		alert('native video supported');
	}).catch(() => {
		alert('native video not supported');
	});

	function checkReady() {
		if (playing && timeupdate && loadedmetadata) {
			copyVideo = true;
			VIDEO_W = VIDEO.videoWidth;
			VIDEO_H = VIDEO.videoHeight;
			updateCanvasSize();
		}
	}
}

//создаем пустую текстуру тк видео не сразу создается(заполнение текстуры кадрами видео выполнено в updateTexture)
function createTexture() {
	//у видео есть задержка и мы просто помещаем 1пиксель в текстуру чтобы мы могли его использовать
	TEXTURE = GL.createTexture();
	GL.activeTexture(GL.TEXTURE0);
	GL.bindTexture(GL.TEXTURE_2D, TEXTURE);
	GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, 1, 1, 0, GL.RGBA, GL.UNSIGNED_BYTE, new Uint8Array([0, 0, 0, 0]));
	//переворачиваем по оси Y
	GL.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, true);
	//заполняем весь холст текстурой
	GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
	GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
	GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
}
//здесь мы запихиваем настоящие фреймы видео в текстуру
function updateTexture() {
	// GL.activeTexture(GL.TEXTURE0);
	GL.bindTexture(GL.TEXTURE_2D, TEXTURE);
	GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, VIDEO);
	GL.uniform1i(GL.getUniformLocation(PROGRAM, 'u_texture'), 0);
}

function updateCanvasSize() {

	let vW = VIDEO_W;
	let vH = VIDEO_H;
	console.log('UPDATE CANVAS SIZE');
	if (!vW || !vH) return;

	let winW = window.innerWidth;
	let winH = window.innerHeight;



	let diffW = Math.abs(winW - vW);
	let diffH = Math.abs(winH - vH);

	let finW, finH;
	console.log({
		vW,
		vH
	}, {
		winW,
		winH
	});
	let inner = (vW <= winW && vH <= winH);
	let outer = (vW >= winW && vH >= winH);
	let heightIn_widthOut = (vW >= winW && vH < winH)
	console.log({
		inner,
		outer,
		'ширина меньше': diffW < diffH,
		heightIn_widthOut,
	})
	//обе стороны меньше/больше контенера
	if (outer || inner) {

		//если оба параметра МЕНЬШЕ контейнера и расстояние между ширинами МЕНЬШЕ чем между высотами, то растягивам до ВЫСОТЫ
		//если оба параметра БОЛЬШЕ контейнера и расстояние между ширинами БОЛЬШЕ чем между высотами, то сжимаем до ВЫСОТЫ
		if ((inner && diffW < diffH) || (outer && diffW >= diffH)) {
			//ширина дальше 
			finH = winH;
			finW = vW * (finH / vH);
		} else {
			console.log(vH, finW / vW);
			finW = winW;
			finH = vH * (finW / vW);
		}
	}
	//какая-то сторона больше, какая-то меньше
	else {
		if (heightIn_widthOut) {
			//растягиваем чтобы высота поместилась
			finH = winH;
			finW = vW * (finH / vH);
		}
		//если высота больше
		else {
			//растягиваем чтобы ширина поместилась
			finW = winW;
			finH = vH * (finW / vW);
		}
	}


	CANVAS.width = finW;
	CANVAS.height = finH;


	GL.viewport(0, 0, GL.canvas.width, GL.canvas.height);
	// GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_canvas_size'), Math.max(CANVAS.width, CANVAS.height));
	GL.uniform2fv(GL.getUniformLocation(PROGRAM, 'u_resolution'), [CANVAS.width, CANVAS.height]);
	// GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_canvas_size'), Math.min(CANVAS.height, CANVAS.width));
}

function initEventListeners() {
	window.addEventListener('resize', updateCanvasSize);
}

function draw(timeStamp) {
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_time'), timeStamp / 1000.0);
	GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);

	if (copyVideo) {
		updateTexture();
	}

	requestAnimationFrame(draw);
}