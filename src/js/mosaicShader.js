//https: //habr.com/ru/post/421821/

import * as dat from 'dat.gui';

const IDs = {
	canvas: 'canvas',
	shaders: {
		vertex: 'mosaicVertShader',
		fragment: 'mosaicFragShader'
	}
};

const URLS = {
	textures: [
		'ruby.jpg',
		'ruby2.jpg',
		'ruby3.jpg',
		'ruby4.jpg',
	]
}


let PROGRAM, MOUSE_POSITION;
let POINTS = [];

const CANVAS = document.getElementById(IDs.canvas);
const GL = canvas.getContext('webgl');
const NUMBER_OF_POINTS = 10;



let userSettings = {
	mosaic: true,
	byGraphic: false,
	showDots: false,
	mouseMask: false,
	mouseMaskCircle: false,
	pixelation: false,
	pixelWaves: false,
}


export default function () {
	clearCanvas();
	createPlane();
	createProgram();
	createTextures();
	updateCanvasSize();
	initEventListeners();
	createPoints();
	setBaseUniforms();
	createGUI();
	draw();
}

function clearCanvas() {
	GL.clearColor(0.26, 1, 0.93, 1.0);
	GL.clear(GL.COLOR_BUFFER_BIT);
}

function createPlane() {
	GL.bindBuffer(GL.ARRAY_BUFFER, GL.createBuffer());
	GL.bufferData(
		GL.ARRAY_BUFFER,
		new Float32Array([
			-1, -1,
			-1, 1,
			1, -1,
			1, 1
		]),
		GL.STATIC_DRAW
	);
}

function createProgram() {
	const shaders = getShaders();

	PROGRAM = GL.createProgram();

	GL.attachShader(PROGRAM, shaders.vertex);
	GL.attachShader(PROGRAM, shaders.fragment);
	GL.linkProgram(PROGRAM);

	const vertexPositionAttribute = GL.getAttribLocation(PROGRAM, 'a_position');

	GL.enableVertexAttribArray(vertexPositionAttribute);
	GL.vertexAttribPointer(vertexPositionAttribute, 2, GL.FLOAT, false, 0, 0);

	GL.useProgram(PROGRAM);
}

function getShaders() {
	return {
		vertex: compileShader(
			GL.VERTEX_SHADER,
			document.getElementById(IDs.shaders.vertex).textContent
		),
		fragment: compileShader(
			GL.FRAGMENT_SHADER,
			document.getElementById(IDs.shaders.fragment).textContent
		)
	};
}

function compileShader(type, source) {
	const shader = GL.createShader(type);

	GL.shaderSource(shader, source);
	GL.compileShader(shader);

	console.log(GL.getShaderInfoLog(shader));

	return shader;
}

function createTextures() {
	for (let i = 0; i < URLS.textures.length; i++) {
		createTexture(i);
	}
}

function createTexture(index) {

	const image = new Image();

	image.crossOrigin = 'anonymous';

	image.onload = () => {
		const texture = GL.createTexture();
		GL.activeTexture(GL['TEXTURE' + index]);
		GL.bindTexture(GL.TEXTURE_2D, texture);
		GL.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, true);
		GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGB, GL.RGB, GL.UNSIGNED_BYTE, image);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);

		GL.uniform1i(GL.getUniformLocation(PROGRAM, 'u_textures[' + index + ']'), index);
	};

	image.src = URLS.textures[index];
}

function updateCanvasSize() {
	const size = Math.ceil(Math.min(window.innerHeight, window.innerWidth));

	CANVAS.height = size;
	CANVAS.width = size;

	GL.viewport(0, 0, GL.canvas.width, GL.canvas.height);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_canvas_size'),
		Math.max(CANVAS.height, CANVAS.width));
}

function initEventListeners() {
	window.addEventListener('resize', updateCanvasSize);

	document.addEventListener('mousemove', (e) => {
		let rect = CANVAS.getBoundingClientRect();

		MOUSE_POSITION = [
			e.clientX - rect.left,
			rect.height - (e.clientY - rect.top)
		];
		GL.uniform2fv(GL.getUniformLocation(PROGRAM, 'u_mouse_position'), MOUSE_POSITION);
	});
}

function createGUI() {
	const gui = new dat.GUI();
	gui.add(userSettings, "mosaic")
	gui.add(userSettings, "byGraphic")
	gui.add(userSettings, "mouseMask")
	gui.add(userSettings, "mouseMaskCircle")
	gui.add(userSettings, "pixelation").listen();
	let pixelWavesController = gui.add(userSettings, "pixelWaves");
	pixelWavesController.onChange(v => {
		if (v) {
			userSettings.pixelation = true;
		}
	})
}

function setBaseUniforms() {

}

function setUpdatedUniforms(timeStamp) {
	for (let i = 0; i < NUMBER_OF_POINTS; i++) {
		GL.uniform2fv(GL.getUniformLocation(PROGRAM, 'u_points[' + i + ']'), POINTS[i]);
	}
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_time'), timeStamp / 1000.0);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_showDots'), userSettings.showDots * 1);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_mosaic'), userSettings.mosaic * 1);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_byGraphic'), userSettings.byGraphic * 1);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_mouseMask'), userSettings.mouseMask * 1);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_mouseMaskCircle'), userSettings.mouseMaskCircle * 1);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_pixelation'), userSettings.pixelation * 1);
	GL.uniform1f(GL.getUniformLocation(PROGRAM, 'u_pixelWaves'), userSettings.pixelWaves * 1);



}

function movePoints(timeStamp) {
	if (timeStamp) {
		for (let i = 0; i < NUMBER_OF_POINTS; i++) {
			POINTS[i][0] += Math.sin(i * timeStamp / 5000.0) / 500.0;
			POINTS[i][1] += Math.cos(i * timeStamp / 5000.0) / 500.0;
		}


		//сравниваем точки по парам и если расстояние между ними меньше предела, то меняем отправляем их в противоположные направление(math.sign)
		for (let i = 0; i < NUMBER_OF_POINTS; i++) {
			for (let j = i; j < NUMBER_OF_POINTS; j++) {
				let deltaX = POINTS[i][0] - POINTS[j][0];
				let deltaY = POINTS[i][1] - POINTS[j][1];
				let distance = Math.sqrt(deltaX ** 2 + deltaY ** 2);

				if (distance < 0.1) {
					// в паре точек у одной из них дельта отрицательная, у другой положительная
					// если дельта положительная, значит наша I-ая точка находится правее, значит ее двигаем вправо
					// а точку с которой сравнивали - влево
					POINTS[i][0] += 0.001 * Math.sign(deltaX);
					POINTS[i][1] += 0.001 * Math.sign(deltaY);
					POINTS[j][0] -= 0.001 * Math.sign(deltaX);
					POINTS[j][1] -= 0.001 * Math.sign(deltaY);
				}
			}
		}

	}
}

function createPoints() {
	for (let i = 0; i < NUMBER_OF_POINTS; i++) {
		POINTS.push([Math.random(), Math.random()]);
	}
}

function draw(timeStamp) {
	movePoints(timeStamp);
	setUpdatedUniforms(timeStamp);
	GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
	requestAnimationFrame(draw);
}