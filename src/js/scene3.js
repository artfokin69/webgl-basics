//СЕТКА + ПЕРЛИН НОЙЗ + АПДЕЙТ ГЕОМЕТРИИ

import * as THREE from 'three';
import Controls from './Controls';
import Perlin from './perlin';

var camera, scene, renderer, mesh, groupX, groupY;
var gridSize = 20;
var time = 0;

function init() {
	scene = new THREE.Scene();
	scene.background = new THREE.Color(0x000000);
	renderer = new THREE.WebGLRenderer();

	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);

	camera = new THREE.PerspectiveCamera(
		90,
		window.innerWidth / window.innerHeight,
		1,
		3000
	)
	camera.position.z = 200;
	camera.position.x = -200;
	camera.position.y = -200;

	Controls({
		scene,
		camera,
		renderer
	});

	document.body.appendChild(renderer.domElement);

	groupX = new THREE.Group();
	groupY = new THREE.Group();

	scene.add(groupX)
	scene.add(groupY)

	let material = new THREE.LineBasicMaterial({
		color: 0xffffff
	});

	for (let j = 0; j < gridSize; j++) {

		let geometryX = new THREE.Geometry();
		let geometryY = new THREE.Geometry();
		for (let i = 0; i < gridSize; i++) {
			geometryX.vertices.push(
				new THREE.Vector3(i * 10, j * 10, 0)
			)
			geometryY.vertices.push(
				new THREE.Vector3(j * 10, i * 10, 0)
			)
		}
		let meshX = new THREE.Line(geometryX, material);
		let meshY = new THREE.Line(geometryY, material);
		groupX.add(meshX)
		groupY.add(meshY)
	}





	animate();
	window.addEventListener('resize', resize);

}

function animate() {
	time++;
	updateGrid();
	window.requestAnimationFrame(animate);
	renderer.render(scene, camera);
}

function resize() {
	renderer.setSize(window.innerWidth, window.innerHeight);
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
}

function updateGrid() {
	for (let i = 0; i < gridSize; i++) {
		let lineX = groupX.children[i];
		let lineY = groupY.children[i];
		for (let j = 0; j < gridSize; j++) {
			let vecX = lineX.geometry.vertices[j];
			let vecY = lineY.geometry.vertices[j];
			vecX.z = 100 * Perlin(vecX.x / 100, vecX.y / 100, time / 100);
			vecY.z = 100 * Perlin(vecY.x / 100, vecY.y / 100, time / 100);
		}
		lineX.geometry.verticesNeedUpdate = true;
		lineY.geometry.verticesNeedUpdate = true;
	}
}










export default init;