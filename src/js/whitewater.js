/*! whitewater-player - v1.0.0 - 2016-07-13 */

export default function Whitewater(a, b, c) {
	"use strict";

	function d() {
		this.canvas.play = this.play, this.canvas.pause = this.pause, this.canvas.playpause = this.playpause, this.canvas.stop = this.stop
	}

	function e() {
		"hidden" in document ? (L = "hidden", document.addEventListener("visibilitychange", u.bind(this), !1)) : "mozHidden" in document ? (L = "mozHidden", document.addEventListener("mozvisibilitychange", u.bind(this), !1)) : "msHidden" in document ? (L = "msHidden", document.addEventListener("msvisibilitychange", u.bind(this), !1)) : "webkitHidden" in document ? (L = "webkitHidden", document.addEventListener("webkitvisibilitychange", u.bind(this), !1)) : "onfocusin" in document ? (document.addEventListener("focusin", u.bind(this, !1), !1), document.addEventListener("focusout", u.bind(this, !0), !1)) : "onpageshow" in window ? (window.addEventListener("pageshow", u.bind(this, !1), !1), window.addEventListener("pagehide", u.bind(this, !0), !1)) : (window.addEventListener("focus", u.bind(this, !1), !1), window.addEventListener("blur", u.bind(this, !0), !1))
	}

	function f() {
		if (D++, D > K.imagesRequired) {
			this.canvas.setAttribute("data-state", "ready"), this.state = "ready";
			var a = new CustomEvent("whitewaterload", h.call(this));
			this.canvas.dispatchEvent(a), c.autoplay && this.play()
		}
	}

	function g() {
		var a = null;
		a = 0 === this.currentFrame ? E : i(H[this.currentFrame - 1]), A.drawImage(a, 0, 0), this.currentFrame++, r.call(this)
	}

	function h() {
		return {
			detail: {
				video: this,
				currentFrame: this.currentFrame,
				progress: this.progress,
				timestamp: this.timestamp,
				maxTime: this.maxTime,
				state: this.state,
				secondsElapsed: this.secondsElapsed
			},
			bubbles: !0,
			cancelable: !1
		}
	}

	function i(a) {
		var b = document.createElement("canvas");
		b.width = K.videoWidth, b.height = K.videoHeight;
		for (var c = 0; c < a.length; c++) {
			var d = a[c][0],
				e = a[c][1],
				f = x(d),
				g = e * K.blockSize;
			if (b.getContext("2d").drawImage(F[B], C.x * K.blockSize, C.y * K.blockSize, g, K.blockSize, f[0] * K.blockSize, f[1] * K.blockSize, g, K.blockSize), C.x += e, C.x >= K.sourceGrid && (C.x = 0, C.y++, C.y >= K.sourceGrid && (C.y = 0, B++, B >= F.length))) throw "imageIndex exceeded diffImages.length\n\nmapLength = " + a.length + "\nj = " + c
		}
		return b
	}

	function j() {
		var a = this;
		E.addEventListener("load", function () {
			f.call(a), q.call(a)
		}, !1), E.src = J + "first." + K.format;
		for (var b = 1; b <= K.imagesRequired; b++) {
			var c = new Image;
			c.addEventListener("load", f.bind(this), !1), b > 99 ? c.src = J + "diff_" + b + "." + K.format : b > 9 ? c.src = J + "diff_0" + b + "." + K.format : c.src = J + "diff_00" + b + "." + K.format, F.push(c)
		}
	}

	function k(a) {
		function b() {
			try {
				I = JSON.parse(f.responseText)
			} catch (a) {
				return void this.constructor._throwError(a)
			}
			t.call(this), s.call(this);
			var b = null,
				d = function () {
					function a(a) {
						for (var b, c = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", d = 0; a.length;) b = c.indexOf(a.charAt(0)), a = a.substr(1), d *= 64, d += b;
						return d
					}
					var b = !1;
					return b = !0, onmessage = function (b) {
						for (var c = b.data, d = [], e = 0; e < c.length; e++) {
							var f = c[e],
								g = [];
							if ("" !== f)
								for (var h = f.match(/.{1,5}/g), i = h.length, j = 0; j < i; j++) {
									var k = a(h[j].substr(0, 3)),
										l = a(h[j].substr(3, 2));
									g.push([k, l])
								}
							d.push(g)
						}
						postMessage(d)
					}, b
				};
			if (d()) {
				var g = window.URL || window.webkitURL,
					h = new Blob(["(" + d.toString() + ")()"], {
						type: "text/javascript"
					});
				b = new Worker(g.createObjectURL(h))
			} else b = new Worker("whitewater.worker.js");
			b.postMessage(I.frames), b.onmessage = function (d) {
				H = d.data, b.terminate();
				for (var f = 0; f < a.length; f++) a[f]();
				c.controls && p.call(e)
			}
		}

		function d() {
			try {
				throw this.constructor.errors.MANIFEST
			} catch (a) {
				return void this.constructor._throwError(a)
			}
		}
		var e = this,
			f = new XMLHttpRequest;
		f.open("GET", J + "manifest.json", !1), f.addEventListener("load", b.bind(this), !1), f.addEventListener("error", d.bind(this), !1), f.send()
	}

	function l() {
		B = 0, C.x = 0, C.y = 0, this.currentFrame = 0, A.clearRect(0, 0, K.videoWidth, K.videoHeight)
	}

	function m() {
		if (!(a instanceof HTMLCanvasElement)) throw this.constructor.errors.CANVAS;
		this.canvas = a, A = this.canvas.getContext("2d")
	}

	function n() {
		if ("string" != typeof b) throw this.constructor.errors.PATH;
		J = b, "/" !== b.substr(-1) && (J += "/")
	}

	function o() {
		if (c) {
			var a = 1;
			c.speed && c.speed < 1 && (a = c.speed), c = {
				loop: c.loop || !1,
				autoplay: c.autoplay || !1,
				controls: c.controls || !1,
				speed: a
			}
		}
	}

	function p() {
		var a = this.canvas;
		"boolean" != typeof c.controls && (a = c.controls);
		var b = w();
		a.addEventListener(b, this.playpause)
	}

	function q() {
		var a = E.src,
			b = this.canvas.style.paddingTop;
		this.canvas.style.background = "transparent url(" + a + ") no-repeat center " + b, this.canvas.style.backgroundSize = "contain"
	}

	function r() {
		this.progress = z(this.currentFrame / K.frameCount * 100, 3);
		var a = this.currentFrame / K.framesPerSecond;
		this.timestamp = y(a), this.secondsElapsed = z(a, 3);
		var b = new CustomEvent("whitewaterprogressupdate", h.call(this));
		this.canvas.dispatchEvent(b)
	}

	function s() {
		this.canvas.setAttribute("width", K.videoWidth + "px"), this.canvas.setAttribute("height", K.videoHeight + "px")
	}

	function t() {
		var a = "";
		switch (I.format) {
			case "JPEG":
				a = "jpg";
				break;
			case "PNG":
				a = "png";
				break;
			case "GIF":
				a = "gif";
				break;
			default:
				a = "jpg"
		}
		K = {
			videoWidth: I.videoWidth,
			videoHeight: I.videoHeight,
			imagesRequired: I.imagesRequired,
			frameCount: I.frameCount - 1,
			blockSize: I.blockSize,
			sourceGrid: I.sourceGrid,
			framesPerSecond: Math.round(I.framesPerSecond),
			format: a
		};
		var b = K.frameCount / K.framesPerSecond;
		this.maxTime = y(b)
	}

	function u(a) {
		void 0 !== a && (M = a), !document[L] && M !== !0 || "playing" !== N.state ? "suspended" === N.state && this.play() : (this.state = "suspended", this.pause())
	}

	function v() {
		try {
			m.call(this), n(), o();
			var a = [j.bind(this), d.bind(this), e.bind(this)];
			k.call(this, a)
		} catch (a) {
			return void this.constructor._throwError(a)
		}
	}

	function w() {
		var a = "ontouchstart" in document.documentElement,
			b = a ? "touchend" : "mouseup";
		return b
	}

	function x(a) {
		var b = [],
			c = Math.ceil(K.videoWidth / K.blockSize);
		return b = a < c ? [a, 0] : [a % c, Math.floor(a / c)]
	}

	function y(a) {
		var b = Math.floor(a / 60),
			c = Math.floor(a % 60),
			d = Math.floor(a % 60 % 1 * 1e3);
		return b < 10 && (b = "0" + b), c < 10 && (c = "0" + c), d < 10 ? d = "00" + d : d < 100 && (d = "0" + d), b + ":" + c + "." + d
	}

	function z(a, b) {
		var c = Math.pow(10, b);
		return Math.round(a * c) / c
	}
	var A = null,
		B = 0,
		C = {
			x: 0,
			y: 0
		},
		D = 0,
		E = new Image,
		F = [],
		G = null,
		H = null,
		I = null,
		J = "",
		K = {},
		L = null,
		M = !1;
	this.state = "loading", this.currentFrame = 0, this.progress = 0, this.timestamp = "00:00.000", this.maxTime = "00:00.000", this.secondsElapsed = 0, Whitewater.supported && v.call(this);
	var N = this;
	this.pause = function () {
		if ("paused" !== N.state) {
			if ("suspended" !== N.state) {
				N.canvas.setAttribute("data-state", "paused"), N.state = "paused";
				var a = new CustomEvent("whitewaterpause", h.call(N));
				N.canvas.dispatchEvent(a)
			}
			cancelAnimationFrame(G)
		}
	}, this.play = function () {
		function a(b) {
			var d = b - i;
			if (d >= f) {
				if (N.currentFrame < K.frameCount + 1) g.call(N);
				else if (c.loop) {
					l.call(N), g.call(N);
					var e = new CustomEvent("whitewaterloop", h.call(N));
					N.canvas.dispatchEvent(e)
				} else {
					N.stop(), N.canvas.setAttribute("data-state", "ended"), N.state = "ended";
					var j = new CustomEvent("whitewaterend", h.call(N));
					N.canvas.dispatchEvent(j)
				}
				var k = d - f;
				i = b - k
			}
			document[L] || M === !0 || "playing" !== N.state || (G = requestAnimationFrame(a))
		}
		if ("playing" !== N.state) {
			"ended" === N.state && l.call(N);
			var b = "suspended" === N.state;
			if (N.canvas.setAttribute("data-state", "playing"), N.state = "playing", !b) {
				var d = new CustomEvent("whitewaterplay", h.call(N));
				N.canvas.dispatchEvent(d)
			}
			var e = 1 / K.framesPerSecond * 1e3,
				f = z(e / c.speed, 2),
				i = window.performance.now();
			a(i)
		}
	}, this.playpause = function () {
		"playing" === N.state ? N.pause() : "loading" !== N.state && N.play()
	}, this.stop = function () {
		if ("ready" !== N.state) {
			N.canvas.setAttribute("data-state", "ready"), N.state = "ready";
			var a = new CustomEvent("whitewaterend", h.call(N));
			N.canvas.dispatchEvent(a), cancelAnimationFrame(G), l.call(N), r.call(N)
		}
	}
}
Whitewater.errors = {
	pre: "Whitewater: ",
	MISC: "Whatever.",
	WEBWORKERS: "This browser does not support Web Workers.",
	BLOBCONSTRUCTOR: "This browser does not support the Blob() constructor.",
	VISIBILITYAPI: "This browser does not support the Visiblity API",
	CANVAS: '"canvas" must be a valid HTML canvas element.',
	PATH: '"path" must be a path to a directory containing a manifest.json file',
	MANIFEST: "A manifest.json file could not be found."
}, Whitewater._checkSupport = function () {
	try {
		if (window.Blob) {
			if (window.Worker) {
				if ("hidden" in document || "mozHidden" in document || "msHidden" in document || "webkitHidden" in document) return !0;
				throw this.errors.VISIBILITYAPI
			}
			throw this.errors.BLOBCONSTRUCTOR
		}
		throw this.errors.WEBWORKERS
	} catch (a) {
		return this._throwError(a), !1
	}
}, Whitewater._throwError = function (a) {
	console.warn(this.errors.pre + a)
}, Whitewater.supported = Whitewater._checkSupport();