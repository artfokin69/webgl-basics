import * as THREE from 'three';
import Controls from './Controls';

var camera, scene, renderer, cube, plane;
var curve2d = {
	mesh: false,
	curve: false,
	count: 20,
}
var t = 0;

function animate() {
	t++;
	animateGround(plane.geometry, plane.material.map);
	// updateCurve2d()
	// twist(cube.geometry);



	window.requestAnimationFrame(animate);
	renderer.render(scene, camera);
}

export default function () {
	camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
	camera.position.set(0, 5, 5)
	scene = new THREE.Scene();

	renderer = new THREE.WebGLRenderer();
	// renderer.setClearColor('#f0f0f0');
	renderer.setClearColor('#000000');
	Controls({
		scene,
		camera,
		renderer
	});
	resize();
	document.body.appendChild(renderer.domElement);

	// ELEMENTS


	var geometry = new THREE.PlaneGeometry(5, 5, 300, 300);
	var texture = THREE.ImageUtils.loadTexture('lines-2k.jpg');
	texture.center = new THREE.Vector2(0.5, 0.5);
	texture.rotation = -1;
	// texture.wrapS = THREE.RepeatWrapping;
	// texture.wrapT = THREE.RepeatWrapping;
	texture.wrapT = texture.wrapS = 1;

	var material = new THREE.MeshBasicMaterial({
		// color: 0xff0000,
		side: THREE.DoubleSide,
		combine: THREE.AddOperation,
		// wireframe:true,
		map: texture,
		skinning: true,
	});
	plane = new THREE.Mesh(geometry, material);
	// plane.position.y = 1; 
	plane.rotation.x = -Math.PI / 2;
	scene.add(plane);
	animateGround(plane.geometry, plane.material.map);
	// !ELEMENTS


	animate();
	window.addEventListener('resize', resize);
}




function twist(geometry) {
	const quaternion = new THREE.Quaternion();

	for (let i = 0; i < geometry.vertices.length; i++) {
		// a single vertex Y position
		const yPos = geometry.vertices[i].y;
		const twistAmount = 10;
		const upVec = new THREE.Vector3(0, 1, 0);

		quaternion.setFromAxisAngle(
			upVec,
			(Math.PI / 180) * (yPos / twistAmount)
		);

		geometry.vertices[i].applyQuaternion(quaternion);
	}

	// tells Three.js to re-render this mesh
	geometry.verticesNeedUpdate = true;
}

function animateGround(geometry, texture) {
	geometry.vertices.forEach((particle, index) => {
		var dX, dY, dZ;
		dX = Math.sin(t / 100 + index / 16) / 6;
		dY = 0;
		dZ = 0;
		particle.z = -1 * dX;

		// particle.z = -1*Math.random()*2;
	});
	geometry.verticesNeedUpdate = true;
	// texture.needsUpdate = true;
}



function resize() {
	renderer.setSize(window.innerWidth, window.innerHeight);
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
}

function genCurve() {
	var points = []
	for (var x = 0; x < curve2d.count; x++) {
		points.push(new THREE.Vector2(x, Math.sin(x + t / 10) * 2));
	}
	return new THREE.SplineCurve(points);
}

function updateCurve2d() {
	scene.remove(curve2d.mesh);
	var curve = genCurve();
	var points = curve.getPoints(1000);
	var geometry = new THREE.BufferGeometry().setFromPoints(points);
	var material = new THREE.LineBasicMaterial({
		color: 0x000000
	});
	var splineObject = new THREE.Line(geometry, material);
	curve2d.mesh = splineObject;
	curve2d.curve = curve;
	scene.add(splineObject);
}

function loadSvg() {
	var loader = new SVGLoader();

	loader.load(
		'line.svg',
		data => {
			var paths = data.paths;
			var group = new THREE.Group();
			group.scale.multiplyScalar(0.1);
			group.position.set(0, 0, 0)


			for (var i = 0; i < paths.length; i++) {
				var path = paths[i];
				var fillColor = path.userData.style.fill;
				if (fillColor !== undefined && fillColor !== 'none') {
					var material = new THREE.MeshBasicMaterial({
						color: new THREE.Color().setStyle(fillColor),
						opacity: path.userData.style.fillOpacity,
						transparent: path.userData.style.fillOpacity < 1,
						side: THREE.DoubleSide,
						depthWrite: false,
						wireframe: false,
					});
					var shapes = path.toShapes(true);
					for (var j = 0; j < shapes.length; j++) {
						var shape = shapes[j];
						var geometry = new THREE.ShapeBufferGeometry(shape);
						var mesh = new THREE.Mesh(geometry, material);
						group.add(mesh);
					}
				}
				var strokeColor = path.userData.style.stroke;
				if (strokeColor !== undefined && strokeColor !== 'none') {
					var material = new THREE.MeshBasicMaterial({
						color: new THREE.Color().setStyle(strokeColor),
						opacity: path.userData.style.strokeOpacity,
						transparent: path.userData.style.strokeOpacity < 1,
						side: THREE.DoubleSide,
						depthWrite: false,
						wireframe: false
					});
					for (var j = 0, jl = path.subPaths.length; j < jl; j++) {
						var subPath = path.subPaths[j];
						var geometry = SVGLoader.pointsToStroke(subPath.getPoints(), path.userData.style);
						if (geometry) {
							var mesh = new THREE.Mesh(geometry, material);
							group.add(mesh);
						}
					}
				}
			}
			scene.add(group);
		},
		// called when loading is in progresses
		function (xhr) {

			// console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

		},
		// called when loading has errors
		function (error) {

			// console.log( 'An error happened' );

		}
	)
}