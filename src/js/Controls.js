import OrbitControls from 'three-orbitcontrols';
import {
	AxesHelper,
	GridHelper
} from 'three';
export default function (p) {
	// scene, camera, renderer, orbit = true, grid, axes
	var controls, axesHelper, gridHelper;
	if (p.orbit !== false) {
		controls = new OrbitControls(p.camera, p.renderer.domElement);
		controls.target.set(0, 1, 0);
		controls.update();
		var xSpeed = 10;
		var ySpeed = 10;

		document.addEventListener("keydown", onDocumentKeyDown, false);

		function onDocumentKeyDown(event) {
			var keyCode = event.which;
			if (keyCode == 87) {
				p.camera.position.y += ySpeed;
			} else if (keyCode == 83) {
				p.camera.position.y -= ySpeed;
			} else if (keyCode == 65) {
				p.camera.position.x -= xSpeed;
			} else if (keyCode == 68) {
				p.camera.position.x += xSpeed;
			} else if (keyCode == 32) {
				p.camera.position.set(0, 0, 0);
			}
		};
	}
	if (p.axes) {
		axesHelper = new AxesHelper(p.axes);
		p.scene.add(axesHelper);
	}
	if (p.grid) {
		gridHelper = new GridHelper(p.gird.w || 10, p.gird.h || 10);
		// helper.rotation.x = Math.PI / 2;
		p.scene.add(gridHelper);
	}
	if (p.meshRotate) {
		//вращаем нажимая x(88),y(89),z(90), в обратную сторону зажимаем shift(16)
		//и на r(82) - reset

		let speed = 0.1;
		let mesh = p.meshRotate;
		let originalRotation = mesh.rotation.clone();
		let shiftPressedCoef = 1;
		document.addEventListener("keydown", () => {
			let keyCode = event.which;
			switch (keyCode) {
				case 16:
					shiftPressedCoef = -1;
					break;
				case 90: //z
					mesh.rotation.z += speed * shiftPressedCoef;
					break;
				case 89: //y
					mesh.rotation.y += speed * shiftPressedCoef;
					break;
				case 88: //x
					mesh.rotation.x += speed * shiftPressedCoef;
					break;
				case 82:
					mesh.rotation.set(originalRotation.x, originalRotation.y, originalRotation.z);
					break;
			}
		}, false);
		document.addEventListener("keyup", () => {
			let keyCode = event.which;
			if (keyCode == 16) {
				shiftPressedCoef = 1;
			}
		}, false);
	}

	return {
		controls,
		axesHelper,
		gridHelper
	};

}