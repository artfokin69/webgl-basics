import * as THREE from 'three';
import * as dat from 'dat.gui';
import {
	TimelineLite
} from 'gsap';
import Controls from './Controls';
var OBJLoader = require('three-obj-loader');
OBJLoader(THREE);
var MTLLoader = require('three-mtl-loader');
import CSS3DRendererInit from './CSS3DRenderer';
CSS3DRendererInit(THREE);


let SCENE;
let CAMERA;
let RENDERER;
let LOADING_MANAGER;
let IMAGE_LOADER;
let TEXTURE_LOADER;
let OBJ_LOADER;
let MTL_LOADER;
let CONTROLS;
let MOUSE;
let RAYCASTER;
let CSSRENDERER;
let TEXTURE;
let OBJECT;
const _IS_ANIMATED = Symbol('is animated');
const _IS_VISIBLE = Symbol('is visible');

function initScene() {
	SCENE = new THREE.Scene();
	// SCENE.background = new THREE.Color(0x000000);
	initLights();

	CAMERA = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
	CAMERA.position.z = 100;

	RAYCASTER = new THREE.Raycaster();
	MOUSE = new THREE.Vector2();

}

function initLights() {
	const ambient = new THREE.AmbientLight(0xffffff, 0.7);
	SCENE.add(ambient);

	const directionalLight = new THREE.DirectionalLight(0xffffff);
	directionalLight.position.set(0, 1, 1);
	SCENE.add(directionalLight);
}

function initRenderer() {
	RENDERER = new THREE.WebGLRenderer({
		alpha: true
	});
	RENDERER.setPixelRatio(window.devicePixelRatio);
	RENDERER.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(RENDERER.domElement);
}

function initLoaders() {
	LOADING_MANAGER = new THREE.LoadingManager();
	IMAGE_LOADER = new THREE.ImageLoader(LOADING_MANAGER);
	TEXTURE_LOADER = new THREE.TextureLoader(LOADING_MANAGER);
	OBJ_LOADER = new THREE.OBJLoader(LOADING_MANAGER);
	MTL_LOADER = new MTLLoader(LOADING_MANAGER);
}

function loadModel() {
	OBJ_LOADER
		.load('model.obj', object => {
			object.traverse(function (child) {
				if (child.isMesh) {
					switch (child.material.name) {
						case 'Christmas_Tree':
							child.material.map = TEXTURE;
							break;
						case 'red':
							child.material.color.setHSL(Math.random(), 1, 0.5);
							break;
						case 'pink':
							child.material.color.setHSL(Math.random(), 1, 0.5);
							break;
					}
				}
			});
			object.scale.x = 0.3;
			object.scale.y = 0.3;
			object.scale.z = 0.3;
			object.rotation.x = -Math.PI / 2;
			object.position.y = -30;

			OBJECT = object;
			SCENE.add(OBJECT);
		})
}


function loadTexture() {
	TEXTURE_LOADER.load('texture.jpg', (texture) => {
		TEXTURE = texture;
	});
}

function initWorld() {
	const geometry = new THREE.SphereGeometry(500, 64, 64);
	geometry.scale(-1, 1, 1);
	const texture = new THREE.Texture();
	const material = new THREE.MeshBasicMaterial({
		map: texture
	});

	IMAGE_LOADER.load('./world.jpg', (image) => {
		texture.image = image;
		texture.needsUpdate = true;
	});
	let sphere = new THREE.Mesh(geometry, material);
	SCENE.add(sphere)
}

function onMouseMove(event) {

	// calculate mouse position in normalized device coordinates
	// (-1 to +1) for both components

	MOUSE.x = (event.clientX / window.innerWidth) * 2 - 1;
	MOUSE.y = -(event.clientY / window.innerHeight) * 2 + 1;

}

function paintHoveredBalls() {
	if (OBJECT) {
		const intersects = RAYCASTER.intersectObjects(OBJECT.children);
		for (let i = 0; i < intersects.length; i++) {
			let mesh = intersects[i].object;
			switch (mesh.material.name) {
				case 'red':
				case 'pink':
					if (!mesh[_IS_ANIMATED]) {
						mesh[_IS_ANIMATED] = true;
						new TimelineLite({
								onComplete: () => {
									mesh[_IS_ANIMATED] = false;
								}
							})
							.to(mesh.material.color, 1, {
								r: Math.random(),
								g: Math.random(),
								b: Math.random(),
								ease: Linear.easeNone,
							}, '0')

					}
					break;
			}
		}
	}
}

function initCSSRenderer() {
	CSSRENDERER = new THREE.CSS3DRenderer();
	CSSRENDERER.setSize(window.innerWidth, window.innerHeight);
	CSSRENDERER.domElement.style.position = 'absolute';
	CSSRENDERER.domElement.style.top = 0;
}

function initPopups() {
	const popupSource = document.querySelector('.popup-3d');


	// popupSource[_IS_VISIBLE] = true;
	const popup = new THREE.CSS3DObject(popupSource);
	popup.position.x = 0;
	popup.position.y = -10;
	popup.position.z = 30;
	popup.scale.x = 0.05;
	popup.scale.y = 0.05;
	popup.scale.z = 0.05;

	SCENE.add(popup);
}


function initEventListeners() {
	window.addEventListener('resize', onWindowResize);
	window.addEventListener('mousemove', onMouseMove, false);
	onWindowResize();
}

function onWindowResize() {
	CAMERA.aspect = window.innerWidth / window.innerHeight;
	CAMERA.updateProjectionMatrix();

	RENDERER.setSize(window.innerWidth, window.innerHeight);
	CSSRENDERER.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
	requestAnimationFrame(animate);
	render();
}

function render() {
	RAYCASTER.setFromCamera(MOUSE, CAMERA);
	paintHoveredBalls();
	CAMERA.lookAt(SCENE.position);
	RENDERER.render(SCENE, CAMERA);
	CSSRENDERER.render(SCENE, CAMERA);
}

export default function () {
	initScene();
	initCSSRenderer();
	initRenderer();
	initLoaders();
	loadTexture();
	initWorld();
	loadModel();

	initPopups();
	initEventListeners();

	animate();
	Controls({
		scene: SCENE,
		camera: CAMERA,
		renderer: RENDERER,
	})
};