import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';
import {MeshLine,MeshLineMaterial} from 'three.meshline';

var camera, scene, renderer, controls, light;
var t = 0;
var lineMesh,lineMaterial;
var pointsCount = 10;



function animate(){
	t++;
	createLine();	
	window.requestAnimationFrame(animate);
	renderer.render(scene, camera);
}

function resize(){
	renderer.setSize( window.innerWidth, window.innerHeight );
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
}
function addControls(){
	controls = new OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 1, 0 );
	controls.update();

	var axisHelper = new THREE.AxesHelper( 10 );
	scene.add( axisHelper );
}
function getNewPoints(progress){
	var points = [];
	for (let i = 0; i < pointsCount; i++) {
		points.push(new THREE.Vector3(i, Math.sin(t/100 + i) * i , 1));
	}
	return points;
}
function createLine(){
	scene.remove( lineMesh );
	var points = getNewPoints();
	var linePoints = new THREE.Geometry().setFromPoints(new THREE.CatmullRomCurve3(points).getPoints(50));
	var line = new MeshLine();
	line.setGeometry( linePoints , p=>1);
	lineMesh = new THREE.Mesh( line.geometry, lineMaterial );
	scene.add( lineMesh );
	
}

export default function(){
	camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
	camera.position.set(0,5,5)
	scene = new THREE.Scene();
	// scene.background = new THREE.Color(0xf0f0f0);
	renderer = new THREE.WebGLRenderer();
	renderer.setClearColor('#e5e5e5');
	addControls();
	resize();
	document.body.appendChild( renderer.domElement );

	// var geometry = new THREE.BoxGeometry(1,1,1);
	// var material = new THREE.MeshLambertMaterial( {color: 0xff0f0ff} );
	// var cube = new THREE.Mesh(geometry,material);
	// cube.position.set(1,1,1)
	// scene.add(cube);

	// var groundGeometry = new THREE.PlaneBufferGeometry( 9, 9, 1, 1 );
	// var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xa0adaf, shininess: 150 } );
	// var ground = new THREE.Mesh(groundGeometry,groundMaterial);
	// ground.rotation.x = - Math.PI / 2; // rotates X/Y to X/Z
	// ground.receiveShadow = true;
	// scene.add( ground );

	// var lineMaterial= new THREE.MeshBasicMaterial( { color: 0x0000ff } );
	// var lineGeometry = new THREE.Geometry();
	// lineGeometry.vertices.push(new THREE.Vector3( -2, 1, 0) );
	// lineGeometry.vertices.push(new THREE.Vector3( -2, 3, 0) );
	// lineGeometry.vertices.push(new THREE.Vector3( 2, 3, 0) );
	// lineGeometry.vertices.push(new THREE.Vector3( 2, 1, 0) );
	// lineGeometry.vertices.push(new THREE.Vector3( -2, 1, 0) );
	// var line = new THREE.Line( lineGeometry, lineMaterial );
	// scene.add(line);

	// light = new THREE.PointLight(0xffffff, 1, 300)
	// light.position.set(10,10,10);
	// scene.add(light);




	// var geometry = new THREE.BufferGeometry();
	// // create a simple square shape. We duplicate the top left and bottom right
	// // vertices because each vertex needs to appear once per triangle.
	// var vertices = new Float32Array( [
	// 	-1.0, -1.0,  1.0,
	// 	1.0, -1.0,  1.0,
	// 	1.0,  1.0,  1.0,

	// 	1.0,  1.0,  1.0,
	// 	-1.0,  1.0,  1.0,
	// 	-1.0, -1.0,  1.0
	// ] );

	// // itemSize = 3 because there are 3 values (components) per vertex
	// geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
	// var material = new THREE.MeshBasicMaterial( { color: 0xff00ff } );
	// var mesh = new THREE.Mesh( geometry, material );
	// scene.add(mesh)



	// var starsGeometry = new THREE.Geometry();

	// for ( var i = 0; i < 100000; i ++ ) {

	// 	var star = new THREE.Vector3();
	// 	star.x = THREE.Math.randFloatSpread( 2000 );
	// 	star.y = THREE.Math.randFloatSpread( 2000 );
	// 	star.z = THREE.Math.randFloatSpread( 2000 );

	// 	starsGeometry.vertices.push( star );

	// }

	// var starsMaterial = new THREE.PointsMaterial( { color: 0x888888 } );

	// var starField = new THREE.Points( starsGeometry, starsMaterial );

	// scene.add( starField );


	lineMaterial = new MeshLineMaterial({
    transparent: true,
    lineWidth: 0.1,
    color: 0xff0000,
	});
	
	animate();
	window.addEventListener('resize',resize);
}